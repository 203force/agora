// #RANGE_1
class SliderA {
  constructor (rangeElementA, valueElementA, optionsA) {
    this.rangeElementA = rangeElementA
    this.valueElementA = valueElementA
    this.optionsA = optionsA
    this.rangeElementA.addEventListener('input', this.updateSliderA.bind(this))
  }

  init() {
    this.rangeElementA.setAttribute('min', optionsA.min)
    this.rangeElementA.setAttribute('max', optionsA.max)
    this.rangeElementA.value = optionsA.cur
    this.updateSliderA()
  }

  generateBackground(rangeElementA) {   
    if (this.rangeElementA.value === this.optionsA.min) {
      return
    }
    
    let percentage =  (this.rangeElementA.value - this.optionsA.min) / (this.optionsA.max - this.optionsA.min) * 100
    return 'background: linear-gradient(to right, #007AFF, #7CE5E0 ' + percentage + '%, #E8E8E8 ' + percentage + '%, #E8E8E8 100%)'
  }

  updateSliderA (newValue) {
    this.valueElementA.innerHTML = this.rangeElementA.value
    this.rangeElementA.style = this.generateBackground(this.rangeElementA.value)
  }
}

let rangeElementA = document.querySelector('.range #range_1 [type="range"]')
let valueElementA = document.querySelector('.range #range_1 .bubble') 

let optionsA = {
  min: 0,
  max: 10,
  cur: 0
}

if (rangeElementA) {
  let sliderA = new SliderA(rangeElementA, valueElementA, optionsA)
  sliderA.init()
}


const rangeA = document.querySelectorAll("#range1content");

rangeA.forEach(wrap => {
  const range = wrap.querySelector(".rangeValue");
  const bubble = wrap.querySelector(".bubble");

  range.addEventListener("input", () => {
    setBubbleA(range, bubble);
  });
  setBubbleA(range, bubble);
});

function setBubbleA(range, bubble) {
  const val = range.value;
  const min = range.min ? range.min : 0;
  const max = range.max ? range.max : 10;
  const newVal = Number(((val - min) * 100) / (max - min));
  bubble.innerHTML = val;
  bubble.style.left = `calc(${newVal}% + (${12 - newVal * 0.25}px))`;
}


// #RANGE_2
class SliderB {
  constructor (rangeElementB, valueElementB, optionsB) {
    this.rangeElementB = rangeElementB
    this.valueElementB = valueElementB
    this.optionsB = optionsB
    this.rangeElementB.addEventListener('input', this.updateSliderB.bind(this))
  }

  init() {
    this.rangeElementB.setAttribute('min', optionsB.min)
    this.rangeElementB.setAttribute('max', optionsB.max)
    this.rangeElementB.value = optionsB.cur
    this.updateSliderB()
  }

  generateBackground(rangeElementB) {   
    if (this.rangeElementB.value === this.optionsB.min) {
      return
    }
    
    let percentage =  (this.rangeElementB.value - this.optionsB.min) / (this.optionsB.max - this.optionsB.min) * 100
    return 'background: linear-gradient(to right, #007AFF, #7CE5E0 ' + percentage + '%, #E8E8E8 ' + percentage + '%, #E8E8E8 100%)'
  }

  updateSliderB (newValue) {
    this.valueElementB.innerHTML = this.rangeElementB.value
    this.rangeElementB.style = this.generateBackground(this.rangeElementB.value)
  }
}

let rangeElementB = document.querySelector('.range #range_2 [type="range"]')
let valueElementB = document.querySelector('.range #range_2 .bubble') 

let optionsB = {
  min: 0,
  max: 10,
  cur: 0
}

if (rangeElementB) {
  let sliderB = new SliderB(rangeElementB, valueElementB, optionsB)
  sliderB.init()
}

const rangeB = document.querySelectorAll("#range2content");

rangeB.forEach(wrap => {
  const range = wrap.querySelector(".rangeValue");
  const bubble = wrap.querySelector(".bubble");

  range.addEventListener("input", () => {
    setBubble(range, bubble);
  });
  setBubble(range, bubble);
});

function setBubble(range, bubble) {
  const val = range.value;
  const min = range.min ? range.min : 0;
  const max = range.max ? range.max : 10;
  const newVal = Number(((val - min) * 100) / (max - min));
  bubble.innerHTML = val;
  bubble.style.left = `calc(${newVal}% + (${12 - newVal * 0.25}px))`;
}


